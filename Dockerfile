FROM python:3.6

RUN pip install .
EXPOSE 8080

CMD resizer --port=8080