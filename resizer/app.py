"""
Image Resizer Service

Usage:
   resizer [--host=HOST] [--port=PORT] [--uvloop] [--log=LOGLEVEL] [--processpool] [--fast]

Options:
   -a host --host=HOST    Listen on this address [default: 0.0.0.0]
   -p PORT --port=PORT    Listen on this port [default: 8080]
   --uvloop               Use the faster uvloop event loop (requires installing uvloop)
   --log=LOGLEVEL         Set log verbosity to this [default: warning]
   --processpool          Use a process pool instead of a thread pool
   --fast                 Resize to approximate dimensions depending on input file. Faster.

"""
import asyncio
import io
import logging
import sys
from concurrent.futures import ProcessPoolExecutor
from typing import Optional, Tuple

import aiohttp
from aiohttp import web
from docopt import docopt
from PIL import Image

PORTRAIT_DIMENSIONS = (320, 568)
LANDSCAPE_DIMENSIONS = (640, 480)
DEFAULT_RESAMPLE = Image.BICUBIC

# Pre-load all drivers instead of lazy-loading as-needed. This fills the Image.MIME registry.
Image.init()


def resize_image(data: bytes, dimensions: Optional[Tuple[int, int]] = None,
                 use_thumbnail: bool = False) -> Tuple[bytes, str]:
    """
    Resize an image. Should only be called from an executor, as this is a blocking call.

    :param data: bytes of image data from the client
    :param dimensions: Specific dimensions to resize to, or None to use defaults
    :param use_thumbnail: Use the faster but less... accurate... Image.thumbnail function
    :return: A tuple of (image_data, image_format_code)
    """
    data_reader = io.BytesIO(data)

    with Image.open(data_reader) as img:
        if dimensions is None:
            dimensions = PORTRAIT_DIMENSIONS if img.height > img.width else LANDSCAPE_DIMENSIONS

        image_format = img.format
        if not use_thumbnail:
            img = img.resize(dimensions, DEFAULT_RESAMPLE)
        else:
            img.thumbnail(dimensions, DEFAULT_RESAMPLE)

        output = io.BytesIO()
        img.save(output, format=image_format)

        return output.getvalue(), image_format


async def get_image(url: str) -> bytes:
    """
    Fetch the contents of url and return the bytes. On network failures, or if the
    given URL is invalid, a HTTPInternalServerError is raised.
    """
    async with aiohttp.ClientSession() as session:
        try:
            async with session.get(url) as response:
                response.raise_for_status()
                return await response.read()
        except (aiohttp.ClientError, ValueError) as e:
            raise web.HTTPInternalServerError(reason='Could not fetch image from URL') from e


async def resize(request: web.Request):
    if request.method == 'GET':
        # If the request is a GET request grab the URL from the querystring and fetch it
        #
        if 'url' not in request.query:
            return web.HTTPBadRequest(reason='No image URL given')

        # Fetch the image data
        image_url = request.query['url']
        image_data = await get_image(image_url)
    else:
        # For POST requests grab only the first multipart entry and read it's content.
        multipart = await request.multipart()
        multipart_image = await multipart.next()

        if multipart_image is None:
            return web.HTTPBadRequest(reason='No image present')

        image_data = await multipart_image.read()

    dimensions = None

    if 'height' in request.query and 'width' in request.query:
        request_height, request_width = request.query['height'], request.query['width']
        if not request_height.isdigit() or not request_width.isdigit():
            return web.HTTPBadRequest(reason='Height and width are not integers')

        dimensions = (int(request_width), int(request_height))

    try:
        resized_data, image_format = await request.app.loop.run_in_executor(None, resize_image, image_data, dimensions,
                                                                            request.app.fast_resize)
    except (OSError, ValueError):
        # Pillow seems to throw ValueError or OSError randomly, and not a library-defined exception (booo!).
        # Not great, but this is better than just `except Exception`
        return web.HTTPBadRequest(reason='Invalid image or encountered an error while resizing')

    return web.Response(body=resized_data, content_type=Image.MIME[image_format])


def main():
    opt = docopt(__doc__)

    try:
        port = int(opt['--port'])
    except ValueError:
        print(f"Port value {opt['--port']} is not an integer", file=sys.stderr)
        exit(1)

    if opt['--uvloop']:
        try:
            import uvloop
        except ImportError as e:
            print(f'Could not import the uvloop package: {e}. Using default loop', file=sys.stderr)
        else:
            asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

    if opt['--processpool']:
        asyncio.get_event_loop().set_default_executor(ProcessPoolExecutor())

    logger = logging.getLogger('aiohttp.web')

    log_level = opt['--log'].upper()

    # Eww, private variables. Must be a better way to do this...
    if log_level not in logging._nameToLevel:
        print(f'Unknown log level: {log_level}', file=sys.stderr)
    else:
        logger.setLevel(logging._nameToLevel[log_level])

    logger.addHandler(logging.StreamHandler())

    app = web.Application(logger=logger)
    app.router.add_route('POST', '/resize', resize)
    app.router.add_route('GET', '/resize', resize)

    # Hack to pass state from the app to the views
    app.fast_resize = opt['--fast']

    print(f'Hit localhost:{port}/resize to begin resizing.')

    web.run_app(app, host=opt['--host'], port=port)


if __name__ == '__main__':
    main()
