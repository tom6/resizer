from flask import Flask, request, Response
from PIL import Image
import io
import multiprocessing

app = Flask(__name__)


@app.route('/resize', methods=['POST'])
def resize():
    file = request.files['file']

    with Image.open(file.stream) as img:
        resized = img.resize((320, 568), Image.BICUBIC)

        output = io.BytesIO()
        resized.save(output, format=img.format)

        return Response(response=output.getvalue(), mimetype=Image.MIME[img.format])

if __name__ == '__main__':
    app.run(processes=multiprocessing.cpu_count(), port=8080)
