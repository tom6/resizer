#!/usr/bin/env bash

echo Running test ${1}

for f in wrk-scripts/*
do
    base=$(basename $f);
    fname=${base%.*};

    REQUESTS_PS=$(wrk -s $f http://localhost:8080/resize -d 30 -t 4 | tee /dev/null | grep "Requests/sec" | cut -f2 -d ':')
    echo $fname $REQUESTS_PS | tee -a results/${1}.txt
done
