from distutils.core import setup

setup(
    name='resizer',
    version='0.1',
    packages=['resizer'],
    url='',
    license='',
    author='Tom Forbes',
    author_email='tom@tomforb.es',
    description='Reize your images!',
    install_requires=[
        'docopt',
        'pillow',
        'aiohttp',
    ],
    entry_points={
        'console_scripts': [
            'resizer = resizer.app:main'
        ]
    }
)
