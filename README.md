# Resizer

Resizer is my attempt at the Marvel interview challenge. Overall I quite
enjoyed writing this, especially digging into the `pillow` library and
evaluating the numerous different approaches you could take.

## The service

From the specification it seemed like a small, self contained web
service would be ideal. This repository contains a `resizer` python
package. It requires Python 3.6+, and can be installed like so:

    pip3 install --upgrade .

Alternatively there is an *untested* (sorry! No time) `Dockerfile`,
just run:

    docker build . -t resizer && docker run resizer

This will create a `resizer` command you can execute.

    ⇒ resizer --help

    Image Resizer Service

    Usage:
       resizer [--host=HOST] [--port=PORT] [--uvloop] [--log=LOGLEVEL] [--processpool] [--fast]

    Options:
       -a host --host=HOST    Listen on this address [default: 0.0.0.0]
       -p PORT --port=PORT    Listen on this port [default: 8080]
       --uvloop               Use the faster uvloop event loop (requires installing uvloop)
       --log=LOGLEVEL         Set log verbosity to this [default: warning]
       --processpool          Use a process pool instead of a thread pool
       --fast                 Resize to approximate dimensions depending on input file. Faster.

Running `resizer` will spawn a service listening on `8080`.

You can test this using [httpie](https://httpie.org/) like so:

    http -f POST :8080/resize file@benchmark/images/lisa_large.jpg > resized_image.jpg

As described below, the `/resize` endpoint also accepts URLS:

    wget "localhost:8080/resize?url=https://cdn.pixabay.com/photo/2015/11/20/20/54/picture-1053852_1280.jpg" > thumbnail.jpg

And arbitrary sizes:

    wget "localhost:8080/resize?width=100&height=200&url=https://cdn.pixabay.com/photo/2015/11/20/20/54/picture-1053852_1280.jpg" > thumbnail.jpg

The previous two GET requests work in your browser as well.

## The Implementation

I figured an asyncio based implementation would be best. I considered
Django, but for a small, focused service like this I thought it was
a bit overkill.
[Flask would have been a suitable choice](benchmark/flask_example.py),
however with some extensions to the specification that I added (and could see being
useful, explained below) it would be less suitable.

The main reason I considered asyncio as a good candidate is that
the image resizing/resampling algorithms in the `Pillow` image library
are written as C extensions. This is great as it unblocks the GIL while
it executes, allowing Python code to execute concurrently. With an asyncio
service this means the server can be handing other uploads, sending
data or other such things as the image is being resized. With non-async
services like Django or Flask the process is left hanging as the image
is being resised, despite the GIL being released, and cannot handle
other requests.

I evaluated using the `imagemagick` library, so that I could use the
[liquid rescale](https://en.wikipedia.org/wiki/Seam_carving) feature.
This technique can crop images without loosing many of the features,
however it turned out to be too CPU intensive.


### Additional features


I added three extra features to the service. The first is pretty
simple: you can pass an `width` and `height` query parameter to the resize
endpoint and the image will be resized to those dimensions.

The second feature was the ability to accept a `url` query parameter,
and have the service fetch and handle an image at that URL as if it was
uploaded by the client. This could be an Amazon S3 asset, or something
hosted on another network resource. Asyncio can fetch these resources
while handling many other requests, unlike Django/Flask.

As the resized image is returned as the HTTP response this could be
embedded on a page to resize images on-demand (with appropriate
caching), something like:

`<a href='resize.marvelapp.com/?height=300&width=200&url=[ASSET_URL_OR_ID]'>`

The third feature was a 'fast' image resize mode. `Pillow` has a
`thumbnail()` method which returns an image of the *approximate* size
given. To quote the documentation:

    Configures the image file loader so it returns a version of the
    image that as closely as possible matches the given mode and size.

As per the benchmarks below, this method is over 4x as fast as the
exact resize. More information about the nature of the images uploaded
to the service is needed to know if this is suitable.

A feature I did implement and then took out was an interesting one: I
added the ability to upload multiple images in a single request, and
the service would resize them in parallel. As each image finished
resizing it was immediately streamed to the client, potentially in
a different order than was uploaded. This meant less buffering in-memory,
but the performance was not great and the code got rather messy.


## Benchmark

When working on anything performance sensitive I like to measure
and evaluate any choices or changes I make. I wrote a simple benchmark
script that uses [a tool called wrk](https://github.com/wg/wrk) to
simulate sending pictures of the mona-lisa to the service in bulk.

I chose wrk as it turns out both apache-bench and siege (my usual tools
for this job) do not support uploading files. Apache-bench does, but I
could not get it to work in a reasonable time.

The mona-lisa images are:

| Name       | Dimensions  | Size   |
|------------|-------------|--------|
| Very large | 2,835x4,298 | 3.51mb |
| Large      | 1,200x1,788 | 726k   |
| Medium     | 687x1,024   | 199k   |
| Small      | 291x432     | 17k    |

And I benchmarked 7 cases:


| Name                | Command                             |
|---------------------|-------------------------------------|
| Flask               | `flask_example.py` (8 processes)    |
| Base                | `resizer`                           |
| Fast                | `resizer --fast`                    |
| Process             | `resizer --process`                 |
| Uvloop              | `resizer --uvloop`                  |
| Process Fast        | `resizer --process --fast`          |
| Process Fast Uvloop | `resizer --process --fast --uvloop` |

### Results


![](benchmark/graph.png)

